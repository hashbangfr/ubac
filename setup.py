from setuptools import setup, find_packages

setup(
    name="ubac",
    version="0.1.0.dev0",
    description="An easy backup tool",
    author="Arthur Vuillard",
    packages=find_packages(),
    install_requires=[
        'paramiko',
        'ftputil',
    ],
    extras_require={
        ':python_version=="2.6"': ['argparse'],
        'raven': ['raven'],
    },
    entry_points={'console_scripts': ['ubac = ubac.runner:main']},
)
